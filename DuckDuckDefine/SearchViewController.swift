
import UIKit

class SearchViewController: UIViewController {
  @IBOutlet var tableView: UITableView!
  @IBOutlet var searchBar: UISearchBar!
  @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
  
  private lazy var duckDuckGo = DuckDuckGo()
  
  private var searchTerms: [String] = []
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
}

// MARK: - UISearchBarDelegate
extension SearchViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    
    performSearch(for: searchBar.text)
  }
  
  func saveSearchTerm(_ term: String) {
    if searchTerms.contains(term) {
      return
    }
    
    searchTerms.insert(term, at: 0)
    tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .left)
  }
  
  func performSearch(for term: String?) {
    guard let term = term else { return }
    
    activityIndicatorView.startAnimating()
    
    duckDuckGo.performSearch(for: term) { definition in
      self.activityIndicatorView.stopAnimating()
      
      if let definition = definition {
        self.saveSearchTerm(term)
        
        self.performSegue(withIdentifier: "DefinitionSegue", sender: DefinitionSegueContext(definition: definition))
      } else {
        self.showNoDefinitionAlert(for: term)
      }
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let context = sender as? DefinitionSegueContext,
      segue.identifier == "DefinitionSegue" {
      let vc = segue.destination as! DefinitionViewController
      vc.definition = context.definition
    }
  }
  
  func showNoDefinitionAlert(for term: String) {
    let alertController = UIAlertController(title: "Huh...", message: "No definition could be found for \(term). Try searching for something else?", preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
    
    present(alertController, animated: true)
  }
}

// MARK: - UITableViewDataSource
extension SearchViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return searchTerms.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    cell.textLabel!.text = searchTerms[indexPath.row]
    return cell
  }
}

// MARK: - UITableViewDelegate
extension SearchViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    performSearch(for: searchTerms[indexPath.row])
  }
}
