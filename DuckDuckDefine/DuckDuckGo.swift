
import Foundation

// Type: response category, i.e. A (article), D (disambiguation), C (category), N (name), E (exclusive), or nothing.
enum ResultType: String, Codable {
  case article = "A"
  case disambiguation = "D"
  case category = "C"
  case name = "N"
  case exclusive = "E"
}

struct Definition: Codable {
  let title: String
  let description: String
  let resultType: ResultType
  let imageURL: URL?
  
  enum CodingKeys: String, CodingKey {
    case title = "Heading"
    case description = "AbstractText"
    case resultType = "Type"
    case imageURL = "Image"
  }
}

final class DuckDuckGo {
  func performSearch(for term: String, completion: @escaping (Definition?) -> Void) {
  }
}
