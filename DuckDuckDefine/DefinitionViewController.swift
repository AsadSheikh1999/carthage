
import UIKit

class DefinitionViewController: UIViewController {
  @IBOutlet var imageView: RoundedImageView!
  @IBOutlet var descriptionLabel: UILabel!
  @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
  
  var definition: Definition!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = definition.title
    descriptionLabel.text = definition.description
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
}
