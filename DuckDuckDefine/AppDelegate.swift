
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
}

extension UINavigationController {
  open override var childForStatusBarStyle: UIViewController? {
    return topViewController
  }
}

class DefinitionSegueContext {
  let definition: Definition
  
  init(definition: Definition) {
    self.definition = definition
  }
}
