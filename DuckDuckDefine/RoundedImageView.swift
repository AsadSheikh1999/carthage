import UIKit

@IBDesignable
class RoundedImageView: UIImageView {
  override func layoutSubviews() {
    super.layoutSubviews()
    
    layer.masksToBounds = true
    layer.cornerRadius = bounds.width / 2
  }
}
